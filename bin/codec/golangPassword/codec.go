package main

import (
	"strconv"
)

func GeneratePassword(origin string) string {
	const init string = "ddb443343"
	letters := []rune(origin)
	for index := range letters {
		letters[index] = codecLetter(letters[index])
	}
	return init + string(letters)
}

func passToOneDigit(letter rune) rune {
	letterCodecString := strconv.Itoa(int(letter))
	if len(letterCodecString) > 1 {
		firstDigit := string(letterCodecString[1])
		oneDigitLetter, _ := strconv.Atoi(firstDigit)
		return rune(oneDigitLetter)
	}
	return letter
}

func codecLetter(letter rune) rune {
	const codecNum rune = 3
	letterNum := passToOneDigit(getLetterNumber(letter))
	if letterNum%codecNum == 0 {
		return letter
	}
	str := strconv.Itoa(int(passToOneDigit(letterNum + codecNum)))
	return []rune(str)[0]
}

func getLetterNumber(letter rune) rune {
	const asciiInitPos rune = 97
	return letter - asciiInitPos + 1
}
