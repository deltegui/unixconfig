package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Falta el parametro de que quieres codificar!")
	}
	thingToCodec := os.Args[1]
	fmt.Println(GeneratePassword(thingToCodec))
}
