temp_art_dir="/run/media/diego/LinuxData/SteamLibrary/steamapps/compatdata/284160/pfx/drive_c/users/steamuser/AppData/Local/BeamNG.drive/0.24/temp/art"
url="github.com/SnoutBug/BeamNG_terrainMaterialCache/releases/download/default/"

cd $temp_art_dir

wget ${url}etk.tar.gz
wget ${url}jri.tar.gz
wget ${url}utah.tar.gz
wget ${url}derby.tar.gz
wget ${url}hirochi.tar.gz
wget ${url}industrial.tar.gz
wget ${url}gridmap_v2.tar.gz
wget ${url}small_island.tar.gz
ls *.tar.gz | xargs -n 1 tar -xvf
rm *.tar.gz

cd ~
