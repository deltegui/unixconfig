const fs = require("fs");

const transform = value => value.split('\n').join('\\n');

if(process.argv[2]) {
	console.log(transform(process.argv[2]));
} else {
	const data = fs.readFileSync("/dev/stdin", "utf-8");
	console.log(transform(data));
}
