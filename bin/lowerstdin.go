package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	content, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}
	thingToLower := string(content)
	fmt.Print(strings.ToLower(thingToLower))
}
