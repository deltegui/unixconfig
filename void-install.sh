#!/bin/bash

install_cmd="sudo xbps-install"
update_cmd="sudo xbps-install -Syu"

unixconfig_pwd=$(pwd)

cd ~

home_pwd=$(pwd)

ask() {
    echo "$1 [y/N]"
    read response
    if [ $response = "y" ]; then
        $2
    else
        echo "Nope!"
    fi
}

install_synaptics() {
    $install_cmd xf86-input-synaptics
}

install_intel() {
    $install_cmd xf86-video-intel
}

install_nvidia() {
    $install_cmd nvidia
}

install_wm_basics() {
    $install_cmd xwallpaper nnn ranger mupdf maim sxiv slock
}

install_xfce() {
    $install_cmd xfce4-session xfce4-terminal xfwm4 xfce4-plugins mousepad xfce4-settings xfce4-taskmanager Thunar thunar-volman thunar-archive-plugin xfdesktop xfce4-panel xarchiver xfce4-appfinder network-manager-applet
    cp -a $unixconfig_pwd/dotfiles/linux/.xinitrc.xfce ~
}

install_dwm() {
    $install_cmd dmenu libXft-devel libX11-devel libXinerama-devel pkg-config bdftopcf lxappearance

    cp -a $unixconfig_pwd/dotfiles/linux/.xinitrc.dwm $home_pwd/
    ln -s .xinitrc.dwm $home_pwd/.xinitrc

    cd $unixconfig_pwd/suckless/dwm
    make clean; make && sudo cp -a dwm /usr/bin/

    cd $unixconfig_pwd/suckless/st
    make clean; make && sudo cp -a st /usr/bin/

    cd $unixconfig_pwd/suckless/scroll
    make clean; make && sudo cp -a scroll /usr/bin/

    echo "Installing Terminus font..."
    cp -a $unixconfig_pwd/fonts/terminus.tar.gz $home_pwd/
    cd $home_pwd
    tar -xzf ./terminus.tar.gz
    cd ./terminus-font-4.49.1
    make -j8
    sudo make install
    sudo mv /usr/local/share/fonts/terminus /usr/share/fonts/
    sudo rm -rf /usr/local/share/fonts
    fc-cache -fv
    cd $home_pwd
    rm -rf terminus-font-4.49.1/ terminus.tar.gz
}

install_i3() {
    $install_cmd rxvt-unicode i3-gaps i3status
    cp -a $unixconfig_pwd/dotfiles/linux/.config/i3 $home_pwd/.config/
}

cat $unixconfig_pwd/banners/void
echo ""

echo "Updating your system"
`$update_cmd`

echo "Configuring your system..."
ln -s $unixconfig_pwd/dotfiles/generic/.emacs $home_pwd/.emacs
ln -s $unixconfig_pwd/dotfiles/generic/.vim $home_pwd/.vim
cp -a $unixconfig_pwd/dotfiles/generic/.vimrc $home_pwd/.vimrc
cp -a $unixconfig_pwd/dotfiles/generic/.gitconfig $home_pwd/.gitconfig
ln -s $unixconfig_pwd/dotfiles/linux/.Xresources $home_pwd/.Xresources
rm -f ~/.bashrc
ln -s $unixconfig_pwd/dotfiles/linux/.bashrc $home_pwd/.bashrc
mkdir -p $home_pwd/.config
mkdir -p $home_pwd/Pictures
cp -a $unixconfig_pwd/wallpapers/* $home_pwd/Pictures/

echo 'Installing Xorg and basic utils...'
$install_cmd void-repo-nonfree xorg-minimal xorg-server-xephyr xinit xrandr xsetroot xf86-input-libinput xf86-input-evdev pipewire elogind dbus gvim make udisks2 git gcc clang emacs-x11 youtube-dl vlc snooze pavucontrol pamixer ntfs-3g nmap NetworkManager unzip zip curl wget firefox htop setxkbmap gstreamer1 gst-plugins-good1 gst-plugins-base1 gst-plugins-bad1

echo 'Setting DBus'
sudo ln -s /etc/sv/dbus /var/service

echo 'Setting NetworkManager...'
sudo rm /var/service/dhcpcd
sudo ln -s /etc/sv/NetworkManager /var/service/

echo 'Setting fstrim with Snooze weekly'
sudo mkdir -p /etc/cron.weekly/
sudo printf '#!/bin/bash\n\nfstrim /\n' > /etc/cron.weekly/fstrim
sudo ln -s /etc/sv/snooze-weekly /var/service/

ask "Do you have a touchpad?" install_synaptics
ask "Do you have intel integrated graphics?" install_intel
ask "Do you have nvidia graphics? (I will install nvidia package)" install_nvidia

ask "Do you want basic wm utils (xwallpaper, mupdf...)?" install_wm_basics
ask "Do you want DWM?" install_dwm
ask "Do you want i3 WM?" install_i3
ask "Do you want to install xfce4?" install_xfce

echo "Done"
