so ~/.vim/plugins.vim
so ~/.vim/auto.vim

"""""""
" GUI "
"""""""
if has("gui_running")
    "set guifont=Cascadia\ Code\ 12
    set guifont=IBM\ Plex\ Mono\ 11

    " Disable all shitty menus and all stuff in GUI
    set guioptions=
    set noeb vb t_vb=

    set linespace=15 " Line separation please!
    au GUIEnter * set vb t_vb=
endif

"""""""""""
" GENERAL "
"""""""""""
set noswapfile
set modifiable
set nocompatible
syntax on
set number relativenumber
set showmode "Always show current vim mode
set showcmd "Show commands
set tabstop=4 "By default, use 4 spaces for tab
set shiftwidth=4 "Use an ident of 4 spaces
set expandtab "When tab is pressed, insert spaces instead of tabs
set encoding=utf-8 "Use utf-8
set hlsearch "Highlight search

" Set as leader key space
let mapleader=' '
nnoremap <SPACE> <Nop>

"""""""""
" THEME "
"""""""""
" colorscheme one
colorscheme gruvbox
" colorscheme catppuccin_mocha " Darker
" colorscheme catppuccin_macchiato " Dark
" colorscheme catppuccin_frappe " No tan dark
" colorscheme catppuccin_latte " Light

set background=dark " Setting dark/light mode

" This setting is needed for catppuccin
set termguicolors

" CtrlSpace
nnoremap <silent><C-p> :CtrlSpace O<CR>
" ctrlspace needs hidden
set hidden

so ~/.vim/maps.vim
