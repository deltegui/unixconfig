"Enable trailing space highlight
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red

"Highlight current line
set cursorline
hi cursorline cterm=none term=none
autocmd WinEnter * setlocal cursorline
autocmd WinLeave * setlocal nocursorline
highlight CursorLine guibg=#313131 ctermbg=234

"Complete open bracket
inoremap {      {}<Left>
"Complete open and enter bracket
inoremap {<CR>  {<CR>}<Esc>O
inoremap {}     {}

set encoding=utf-8
scriptencoding utf-8
" Show invisible characters (tabs, spaces and EOF)
set listchars=tab:>.,space:.,eol:$
" set listchars=tab:\ \ ,eol:↵
set list

"Command to change default format of C# files
":command Csformat %s/\n *{\n/ {\r/g
