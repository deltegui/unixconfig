" Alternatives to classic ways to save and quit
nnoremap <leader>w :w<cr>
nnoremap <leader>q :q<cr>
nnoremap <space>s :vsplit<cr>

" Can someone tell me why undo is only prssing 'u' and redo is 'Ctrl+r'?
" Because r is for replace a letter
" nnoremap r <C-r>

" Splitted buffers movement
" Now leader + cursor direction change current buffer
nnoremap <leader>h 10<C-w><
nnoremap <leader>l 10<C-w>>
nnoremap <leader>i <C-w>=
nnoremap <leader>j <C-w><left>
nnoremap <leader>k <C-w><right>

" Nerdtree
map <leader>nt :NERDTreeFind<cr>
map <leader>ntc :NERDTreeClose<cr>

" FZF
map <leader>p :Files<cr>
nnoremap <leader>ob :Buffers<cr>
