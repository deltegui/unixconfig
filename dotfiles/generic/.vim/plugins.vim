call plug#begin('~/.vim/plugged')

" Better syntax support
Plug 'sheerun/vim-polyglot'

" Themes
Plug 'rakr/vim-one'
Plug 'morhetz/gruvbox'
Plug 'catppuccin/vim', { 'as': 'catppuccin' }

" Select text and add thing that surrounds that
Plug 'tpope/vim-surround'

" C# Unix backend support
" Plug 'OmniSharp/omnisharp-vim'

" Golang support
Plug 'fatih/vim-go'

" NERDTREE
Plug 'preservim/nerdtree'
let NERDTreeShowHidden=1

" OH GOD THIS IS AWESOME
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

call plug#end()

" Force plugins to load correctly
filetype off
filetype plugin indent on
