vim.o.ma = true
vim.o.mouse = a
vim.o.cursorline = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.expandtab = true
vim.o.autoread = true
vim.o.nu = true
vim.o.relativenumber = true
vim.o.foldlevelstart = 99
vim.o.scrolloff = 7
vim.o.backup = false
vim.o.writebackup = false
vim.o.swapfile = false
vim.g.mapleader = " "

-- manipulate system clipboard (aka +y register)
vim.o.clipboard = 'unnamedplus'
vim.keymap.set('n', '<leader>y', "\"+y")
vim.keymap.set('v', '<leader>y', "\"+y")
vim.keymap.set('n', '<leader>Y', "\"+Y")

-- pls search tune
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- undo things from ages ago
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- omg this is fantastic. move lines up and down if they are highlighted
vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")

-- take a look using <C-d> and <C-u> but keep the cursor in the middle of
-- the screen
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')

-- Show a column indicating the width limit
vim.opt.colorcolumn = "80"

vim.opt.listchars = {
    eol='↵',
    tab='>-',
    space='.',
    trail='•'
}
vim.opt.list = true

vim.keymap.set('n', '<Leader>s', ':vsplit<cr>')
vim.keymap.set('n', '<Leader>w', ':w<cr>')
vim.keymap.set('n', '<Leader>q', ':q<cr>')

vim.keymap.set('n', '<Leader>h', '10<C-w><')
vim.keymap.set('n', '<Leader>l', '10<C-w>>')
vim.keymap.set('n', '<Leader>i', '<C-w>=')
vim.keymap.set('n', '<Leader>j', '<C-w><left>')
vim.keymap.set('n', '<Leader>k', '<C-w><right>')

vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

-- Enable trailing space highlight
-- I dont fucking know how to do it in LUA!!
vim.api.nvim_exec(
[[
    highlight ExtraWhitespace ctermbg=red guibg=red
    match ExtraWhitespace /\s\+$/
    autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhitespace /\s\+$/
    autocmd BufWinLeave * call clearmatches()
    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
]], true)

-- Complete open bracket
vim.cmd("inoremap {      {}<Left>")

-- Complete open and enter bracket
vim.cmd("inoremap {<CR>  {<CR>}<Esc>O")

