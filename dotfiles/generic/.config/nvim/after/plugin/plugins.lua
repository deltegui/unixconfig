-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]
-- run :PackerCompile
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'

    -- Undotree
    use 'mbbill/undotree'

    -- Theme
    use {'catppuccin/nvim', as = 'catppuccin'}

    --Telescope
    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.0',
        requires = {{ 'nvim-lua/plenary.nvim' }}
    }

    -- Languagje Server Protocol things
    use 'hrsh7th/nvim-cmp' -- Completion
    use 'hrsh7th/cmp-nvim-lsp' -- Completion LSP integration
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'L3MON4D3/LuaSnip'
    use 'saadparwaiz1/cmp_luasnip'
    use 'neovim/nvim-lspconfig'
    use 'williamboman/nvim-lsp-installer'

    use('nvim-treesitter/nvim-treesitter', { run = ':TSUpdate' })
    use 'nvim-treesitter/playground'

    --fullstack dev
    -- use 'pangloss/vim-javascript' --JS support
    -- use 'leafgarland/typescript-vim' --TS support
    -- use 'maxmellon/vim-jsx-pretty' --JS and JSX syntax
    use 'mattn/emmet-vim'
end)
