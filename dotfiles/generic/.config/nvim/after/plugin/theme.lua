-- Theme shit
-- latte, frappe, macchiato, mocha (as you can see, all are hipster colors)
-- vim.g.catppuccin_flavour = "macchiato"
require("catppuccin").setup()
vim.cmd [[colorscheme catppuccin]]
-- vim.cmd [[colorscheme dracula]]

vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
