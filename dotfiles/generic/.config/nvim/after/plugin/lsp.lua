-- Native LSP Setup
-- Global setup.
local cmp = require('cmp')

cmp.setup({
    -- Set Snipped Engine
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
    mapping = {
        ['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
        ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<C-e>'] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        }),
        -- Accept currently selected item. If none selected, `select` first item.
        -- Set `select` to `false` to only confirm explicitly selected items.
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' }, -- For luasnip users.
    }, {
        { name = 'buffer' },
    })
})

local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local lsp_installer = require("nvim-lsp-installer")
lsp_installer.settings({
    ui = {
        icons = {
            server_installed = "✓",
            server_pending = "➜",
            server_uninstalled = "✗"
        }
    }
})

lsp_installer.setup{}
local lspconfig = require('lspconfig')
-- lspconfig.gopls.setup {
--     capabilities = capabilities,
--     on_attach = on_attach,
--     settings = {
--         gopls = {
--             gofumpt = true,
--         },
--     },
--     flags = {
--         debounce_text_changes = 150,
--     },
-- }
-- lspconfig.golint.setup {
--     capabilities = capabilities,
--     on_attach = on_attach,
--     settings = {
--         gopls = {
--             gofumpt = true,
--         },
--     },
--     flags = {
--         debounce_text_changes = 150,
--     },
-- }

-- Run goimports and gofmt on save go file
vim.api.nvim_exec(
[[
    autocmd BufWritePost *.go silent !gofmt -w %
    autocmd BufWritePost *.go silent !goimports -w %
    autocmd BufWritePost *.go :e
]], true)


-- Better syntax highlight
require'nvim-treesitter.configs'.setup {
    -- ensure_installed = { "c", "lua", "go" },
    ensure_installed = { "c", "lua" },
    sync_install = false,
    auto_install = true,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
}

-- lspconfig.clangd.setup {
--     on_attach = on_attach,
--     capabilities = capabilities,
-- }

