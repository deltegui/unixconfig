This is a NVim configuration in lua. You dont need NodeJS or other external shit to run NVim.

Installation:

* Copy the nvim folder to your .config folder in your $HOME.
* Install the package manager for NVim -> packer. Just run the command from their github page on your $HOME.
* Open NVim and run PackerInstall
* If you dont have Go and you dont want to use it, comment the autocomplete configuration from .config/nvim/init.lua
* If you dont want C/C++ autocomplete support, comment the configuration from .config/nvim/init.lua.
* If you do want C/C++ autocomplete support you must install:
    * clangd language server
    * something that generates a compile_commands.json. For example CMake or, if you use a Makefile, Bear.
    * Generate a compile_commands.json for your project. You shouldn't commit that file.
    * Enjoy.

If you have used my vim config or my previous NVim config, the shortcuts are the same.
