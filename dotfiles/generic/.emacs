; Display line numbers
(global-display-line-numbers-mode)

; Disable tabs (only spaces)
(setq-default indent-tabs-mode nil)

; Highligh current line
(global-hl-line-mode)

; Mark trailing spaces
(setq-default show-trailing-whitespace t)

; Show whitespaces and EOL
;(global-whitespace-mode 1)

; Disable menubar and toolbar
(menu-bar-mode -1)
(tool-bar-mode -1)

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/")
             t)

(package-initialize)
(load-theme 'gruvbox t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (gruvbox-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'slime-autoloads)
(setq inferior-lisp-program "sbcl")
(setq slime-contribs '(slime-fancy slime-quicklisp slime-asdf))

(defun browse-file-dir ()
  "Open the current file's directory"
  (interactive)
  (if default-directory
      (browse-url-of-file (expand-file-name default-directory))
    (error "No `default directory` to open"))
  )
