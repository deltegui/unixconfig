precmd() {
    local EXIT="$?"                # This needs to be first
    PS1=""

    if [ $EXIT != 0 ]; then
        PS1+="%F{red}λ %f"
    else
        PS1+="%F{green}λ %f"
    fi

    PS1+="%F{white}%~%f$(git_status_prompt)%F{blue} $ %f"
}
#
# A little command that shows your current branch and status when
# you are inside a git repository. Perfect to add to your prompt!
#
# Example (with prompt):
#
# λ unixconfig( master ✓ ) $
#
git_status_prompt() {
    git status --porcelain 2>/dev/null 1>&2
    local exit_code=$?
    if [ $exit_code -eq "0" ]; then
        local branch=$(git branch --show-current 2>/dev/null)
        if [[ `git status --porcelain` ]]; then
            echo "%F{yellow}( %F{blue}$branch %F{red}✗%F{yellow} )"
        else
            echo "%F{yellow}( %F{blue}$branch %F{green}✓%F{yellow} )"
        fi
    fi
}

# NODE VERSION MANAGER THING
lnvm() {
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
}

# My neovim needs Node for autocompletion
svim() {
    if [ -z "$NVM_DIR" ]; then
        echo "Loading NVM..."
        lnvm
    fi
    nvim $1
}

# Git commands
alias gc='git commit -m'
alias gaa='git add .'
alias gp='git push'
alias gl="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias gchk='git checkout'
alias gs='git status'

# Tools
alias ls='ls --color=auto'
alias ll='ls --color=always -lah'
alias lc='ls --color=never'
alias ..='cd ..'

#OSx alias
#alias multone='node ~/bin/multiline.js'
alias brewdep='brew list --formula -1 | while read cask; do echo -ne "\x1B[1;34m $cask \x1B[0m"; brew uses $cask --installed | awk "{printf(\" %s \", \$0)}"; echo ""; done'

# generic recursive chmod.
# - First arg is either 'f' or 'd' to apply chmod to files or directories
# - Second arg is permissions. You usually use 644 for files and 755 for dirs
rchmod() {
    eval "find -type $1 | awk '{ print \"\\\"\"\$0\"\\\"\" }' | xargs chmod $2"
}

# Recursive chmod for directories (set 755)
alias rchmodd="rchmod d 755"
# Recursive chmod for files (set 644)
alias rchmodf="rchmod f 644"
# Fix all permissions recursively
alias fixperm="rchmodd; rchmodf"

# ssh load key
sshlk() {
    eval $(ssh-agent)
    ssh-add ~/.ssh/$1
}

# MAKE BASH GREAT AGAIN!
bindkey 'TAB' menu-complete
bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

export GOPATH=$HOME/go
export GO111MODULE="on"
export PATH=$GOPATH/bin:$PATH
