PS1='λ '

__prompt_command() {
    local EXIT="$?"                # This needs to be first
    PS1=""

    local RCol='\[\e[0m\]'

    local Red='\[\e[0;31m\]'
    local Gre='\[\e[0;32m\]'
    local BYel='\[\e[1;33m\]'
    local BBlu='\[\e[1;34m\]'
    local Pur='\[\e[0;35m\]'

    if [ $EXIT != 0 ]; then
        PS1+="${Red}λ ${RCol}"
    else
        PS1+="${Gre}λ ${RCol}"
    fi

    PS1+="${RCol}${Pur}\W${BYel}$(git_status_prompt) $ ${RCol}"
}

# A little command that shows your current branch and status when
# you are inside a git repository. Perfect to add to your prompt!
#
# Example (with prompt):
#
# λ unixconfig( master ✓ ) $
#
git_status_prompt() {
    git status --porcelain 2>/dev/null 1>&2
    local exit_code=$?
    if [ $exit_code -eq "0" ]; then
        local branch=$(git branch --show-current 2>/dev/null)
        if [[ `git status --porcelain` ]]; then
            echo "${BYel}( ${BBlu}$branch ${Red}✗${BYel} )"
        else
            echo "${BYel}( ${BBlu}$branch ${Gre}✓${BYel} )"
        fi
    fi
}

export PROMPT_COMMAND=__prompt_command    # Function to generate PS1 after CMDs

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export EDITOR=vim
export TERM=xterm-256color

# Git commands
alias gc='git commit -m'
alias gaa='git add .'
alias gp='git push'
alias gl="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias gchk='git checkout'
alias grefresh='git fetch --all && git pull'
alias gs='git status'

# LS
alias ll='ls -lah'
alias lc='ls --color=never'
alias ls='ls --color=auto'

# Alias for Void
#alias xi="sudo xbps-install -S"
#alias up="sudo xbps-install -Su"
#alias xr="sudo xbps-remove -R"
#alias xq="xbps-query -s"
#alias xs="xbps-query -Rs"

# Alias for Arch
# alias xi="sudo pacman -S"
# alias up="sudo pacman -Syu"
# alias xr="sudo pacman -Rs"
# alias xq="pacman -Q"
# alias xs="pacman -Ss"

# Alias for Debian
alias xi="sudo apt install"
alias up="sudo apt update && sudo apt upgrade"
alias xr="sudo apt purge"
alias xq="apt list -a --installed"
alias xs="apt search"

# Handful utils
alias ytdl="youtube-dl"
alias ..="cd .."
alias grep='grep --color=auto'
alias mv='mv -i'
alias rm='rm -i'
alias lsblk='lsblk -fl'
alias genkey='ssh-keygen -t ed25519 -C "$(git config --get user.email)"'

glclone() {
    git clone ssh://diego@192.168.1.69:/home/diego/services/cgit/data/$1
}

# LINUX DWM things
alias picom-start='picom -cCfb --vsync --backend glx'

# generic recursive chmod.
# - First arg is either 'f' or 'd' to apply chmod to files or directories
# - Second arg is permissions. You usually use 644 for files and 755 for dirs
rchmod() {
    eval "find -type $1 | awk '{ print \"\\\"\"\$0\"\\\"\" }' | xargs chmod $2"
}

# Recursive chmod for directories (set 755)
alias rchmodd="rchmod d 755"
# Recursive chmod for files (set 644)
alias rchmodf="rchmod f 644"
# Fix all permissions recursively
alias fixperm="rchmodd; rchmodf"

# ssh load key
sshlk() {
    eval $(ssh-agent)
    ssh-add ~/.ssh/$1
}

# Screen Shot
sshot() {
    maim -d 2 -u -g 1920x1080+0+540 $1
}

# Smart Mount
smount() {
    udisksctl mount -b $1
}

# Smart Unmount
sumount() {
    udisksctl unmount -b $1
    udisksctl power-off -b $1
}

# MAKE BASH GREAT AGAIN!
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# GOLANG shit. Comment to disable
export PATH=$PATH:/usr/local/go/bin
export GOPATH="$HOME/go"
export PATH="$GOPATH/bin:$PATH"

# User binaries
export PATH=$PATH:$HOME/.local/bin

# KEESYNC script data
# export KEESYNC_KDB=Passwords.kdbx
# export KEESYNC_LOCAL=~/Documents/keepass
# export KEESYNC_REMOTE=diego@192.168.1.69:/home/diego

#Execute my random text art (comment to disable)
rndtxtart
